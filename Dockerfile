FROM nicolaka/netshoot:latest

RUN ifconfig -a > /root/output.txt
RUN dig rubygems.org >> /root/output.txt
RUN dig +trace rubygems.org >> /root/output.txt
RUN curl -v --trace-time https://rubygems.org/gems/concurrent-ruby-1.1.9.gem -o concurrent-ruby-1.1.9.gem
